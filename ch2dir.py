#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Author: Inderpreet Singh
"""

import os
from os.path import join, getsize
import sys
import pprint
from optparse import OptionParser
#import mutagen.id3 as id3
import stagger
from stagger.id3 import *

import re, string
  
if __name__ == "__main__":
  """
  Parses command line and renames the directories from delimited to directory based
  My files - personal/ -> My files/personal/
  """
  # create the options we want to parse
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument("dir", nargs=1,
                      help="directory with \" - \" to do the split")
  parser.add_argument("-i", "--ignore", action="store_true",
                      help="Ignore previously created dir")
  args = parser.parse_args()
  
  if len(args.dir) != 1:
    print ("Single directory not specified")
    sys.exit(1)
  else:
    absdir = os.path.abspath(args.dir[0])
    thedir = os.path.basename(absdir)
    print("\nWorking with: %s\n" % (thedir,))

  if not os.path.exists(absdir) or not os.path.isdir(absdir):
    print("Not a directory: %s" % (absdir,))
    sys.exit(2)
  
  if thedir.find(' - '):
    (first, second) = thedir.split(' - ')
    outdir = os.path.join(os.path.dirname(absdir), first)
    
    if not os.path.exists(outdir):
      os.mkdir(outdir)
    elif os.path.exists(outdir) and not args.ignore:
      print("Outside dir already exists: %s. Please delete or --ignore before continuing." % (outdir,))
      sys.exit(3)
    
    if os.path.exists(outdir):
      os.rename(absdir, os.path.join(os.path.dirname(absdir), first, second))
  else:
    print("Delimiter not found")
    sys.exit(4)
    
  # exit successful
  sys.exit(0)
