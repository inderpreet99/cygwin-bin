#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Author: Inderpreet Singh
Date:   2012
Renames files based on the input options.

TODO
- update the line in the array
"""

import os
import sys
import pprint
import re, string
import cmd
import argparse
"""
def findnth(haystack, needle, n):
    parts = haystack.split(needle, n+1)
    print "parts are: "
    print parts
    if len(parts)<n+1:
        return -1
    print "len hastack: %s, lenparts-1: %s, lenneedle: %s" % (len(haystack), len(parts[-1]), len(needle))
    return len(haystack)-len(parts[-1])-len(needle)
"""
def findnth(haystack, needle, n):
    start = haystack.find(needle)
    while start >= 0 and n > 1:
        start = haystack.find(needle, start+len(needle))
        n -= 1
    return start
    
class RenCmd(cmd.Cmd):
    song = -1
    name = ""
    list = []
    undo = dict()
    
    
    def __init__(self, con):
        cmd.Cmd.__init__(self)
        self.maincon = con
        self.prompt = "Ren: "
        self.move()
    
    
    def check_list(self):
        
        # if we dont have the listing, get it
        if not self.list:
            self.list = self.maincon.get_list()
        
        # if we dont have any files to process, exit
        if not self.list:
            print "Nothing found!"
            return True
    
    
    def move(self, prev=False):
        self.check_list();
        
        self.song += (1 if not prev else -1)
        self.song = self.song % len(self.maincon.get_list())
        self.name = self.list[self.song]
        rname = self.maincon.get_ren(self.name, self.maincon.text)
        
        print ""
        print rname
    
    def rename(self, name):
        try:
            print "Renaming to " + name
            os.rename(self.name, name)
            self.name = name
            self.list[self.song] = name
        except OSError, ex:
            print >>sys.stderr, "Error renaming '%s': %s"  % (self.name, ex.strerror)
        self.move()
    
    def do_r(self, line):
        line = line.strip('"')
        name = self.maincon.get_ren(self.name, line)
        try:
            os.rename(self.name, name)
            self.name = name
        except OSError, ex:
            print >>sys.stderr, "Error renaming '%s': %s"  % (self.name, ex.strerror)
        self.move()
        
    
    def do_a(self, line):
        """ Add an artist """
        
        parser = argparse.ArgumentParser()
        parser.add_argument('text', nargs='*')
        parser.add_argument('-r', dest='replace', action='store_true')
        parser.add_argument('-2', dest='second', action='store_true')
        args = parser.parse_args(line.split())
        
        line = ' '.join(args.text).strip('"')
        hyphens = self.name.count(' - ')
        
        if hyphens == 1:
            where = 2
            whereEnd = None
            line = " - " + line
        elif not args.second and hyphens == 2:
            
            # if replacing, find the first " - "
            if args.replace:
                where = self.name.find(' - ') + 3
                whereEnd = self.name.find(' - ', 4)
            else:
                # otherwise appending, so add just before second " - "
                where = self.name.find(' - ', 4)
                whereEnd = None
                line = " & " + line
            
            #name = self.maincon.get_ren(self.name, line, where, whereEnd)
        else:
          if args.replace:
            if args.second:
              where = findnth(self.name, ' - ', 3) + 3
              whereEnd = self.name.lower().find('.mp3')
            else:
              where = self.name.find(' - ') + 3
              whereEnd = self.name.find(' - ', 4)
          else:
            if args.second:
              where = self.name.lower().find('.mp3')
              whereEnd = None
              line = " - " + line
            else:
              # otherwise appending, so add just before second " - "
              where = self.name.find(' - ', 4)
              whereEnd = None
              line = " & " + line
          
        name = self.maincon.get_ren(self.name, line, where, whereEnd)
        #print name
        self.rename(name)
        
    
    def do_t(self, line):
        """ Add title """
        
        parser = argparse.ArgumentParser()
        parser.add_argument('text', nargs='*')
        parser.add_argument('-r', dest='replace', action='store_true')
        args = parser.parse_args(line.split())
        
        line = ' '.join(args.text).strip('"')
        hyphens = self.name.count(' - ')
        
        if hyphens == 1:
            where = self.name.find('.mp3')
            name = self.maincon.get_ren(self.name, " - " + line, where)
        elif hyphens >= 2:
            
            # if replacing, find the first " - "
            if args.replace:
                where = findnth(self.name, ' - ', 2) + 3
                if hyphens == 2:
                  whereEnd = self.name.lower().find('.mp3')
                else:
                  whereEnd = findnth(self.name, ' - ', 3)
            else:
                # otherwise appending
                where = self.name.lower().find('.mp3')
                whereEnd = None
            
            name = self.maincon.get_ren(self.name, line, where, whereEnd)
          
        self.rename(name)
        
    
    def do_n(self, line):
        """ Next """
        self.move()
        
    def do_p(self, line):
        """ Previous """
        self.move(True)
        
    def do_g(self, line):
        """ Go to # """
        
        line = line.strip('"')
        list = self.maincon.get_list()
        found = False
        i = -1
        
        for name in list:
            i += 1
            if line in name:
                found = True
                self.song = i-1
                self.move()
                break
        
        if not found:
            print "Not moving anywhere. Did not find " + name
    
    
    def do_where(self, line):
        self.maincon.do_where(line, self.name)
    
    
    def do_EOF(self, line):
        return True



class HelloWorld(cmd.Cmd):
    """Simple command processor example."""
    
    where = 2
    path = os.path.abspath('.')
    text = '$'
    file = 0
    name = "00 - Test - Song.mp3"
    
    
    def __init__(self):
        cmd.Cmd.__init__(self)
        
        print "\nListing:"
        for name in self.get_list():
            print name
        print ""
    
    
    def get_ren(self, name, text, where=False, end=False):
        if not where:
            where = self.where
        whereint = int(where)
        
        # if we don't have an end, then start+text = end
        if not end:
            end = whereint
            
        rname = name[:whereint] + text + name[end:]
        return rname
        
    
    def check_path(self):
        if not os.path.isdir(self.path):
            print "Not a valid path: %s" % (self.path)
            sys.exit(0)
    
    
    def get_list(self):
        self.check_path()
        
        mp3s = []
        for root, dirs, files in os.walk(self.path):
            for name in files:
                if ".mp3" in name.lower():
                    mp3s.append(name)
        return mp3s
    
    
    def do_start(self, line):
        i = RenCmd(self)
        i.cmdloop()
    
    
#    def do_ren(self, line):
        #whereint = int(self.where)
        #testname = name[:whereint] + self.text + name[whereint:]
        #print "Test: %s" % (testname)
        
        #s = raw_input("Action: ")
        #if s[:1] == '#':
        #print "goto %s" % (s) 
        #elif s == 'r':
        #print "Rend: %s" % (testname)
        #elif s == 'u':
        #print "undo last action"

    
    def do_list(self, line):
        for name in self.get_list():
            print name
    
        
    def do_path(self, line):
        if line:
            self.path = line
            print "Path changed to %s." % (self.path)
        else:
            print "Path: ", self.path
    
    
    def do_where(self, line, name=""):
        if line:
            if line.isdigit():
                self.where = line
            elif line[0] == '+':
                self.where = int(self.where) + int(line[1:])
            elif line[0] == '-':
                self.where = int(self.where) - int(line[1:])
        
        print ""
        print "Where: " , self.where
        print "Test:  " + self.get_ren(name if name else self.name, self.text)
        print ""
    
    
    def do_text(self, line):
        if line:
            self.text = line
        else:
            print "Text: '%s'" % (self.text)
    
    
    def do_EOF(self, line):
        return True



if __name__ == '__main__':
    HelloWorld().cmdloop()